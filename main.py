import os.path

import pandas as pd

import local.dev_config as conf


class ConverisExportParser:
    def __init__(self):
        self.df = self.load_converis_dataframe()
        self.sanitise_dataframe()

    def load_converis_dataframe(self) -> pd.DataFrame:
        """
        Reads an Excel export from Converis (path must be defined in dev_config.py).
        Creates and returns a Pandas dataframe

        :return: Pandas dataframe
        """
        self.df = pd.read_excel(conf.converis_xlsx)
        return self.df

    def sanitise_dataframe(self):
        """
        Remove all the annoying new line characters from the end of every cell
        (https://stackoverflow.com/a/45270483)

        :return: None
        """
        self.df = self.df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    def output_csv(self):
        """
        Outputs dataframe to csv file in the same folder as input file.
        :return: None
        """
        output_filepath = f"{os.path.splitext(conf.converis_xlsx)[0]}_parsed.csv"
        with open(output_filepath, "w") as f:
            self.df.to_csv(f)

    def detect_and_filter_out_preprints(self):
        """

        :return:
        """

        # "Publication: Publication type"
        preprint_publication_types = ["pre-print article"]
        # "Publication: Journal acronym"
        preprint_journal_acronyms = ["biorxiv", "osf preprints"]

        self.df.drop(
            self.df.loc[
                (
                    self.df["Publication: Journal acronym"]
                    .str.lower()
                    .isin(preprint_journal_acronyms)
                    | self.df["Publication: Publication type"]
                    .str.lower()
                    .isin(preprint_publication_types)
                )
            ].index,
            inplace=True,
        )
